import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-video-apresentacao',
  template: `
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ilrBk8SWaF4" allowfullscreen></iframe>
    </div>
  `
})
export class VideoApresentacaoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
