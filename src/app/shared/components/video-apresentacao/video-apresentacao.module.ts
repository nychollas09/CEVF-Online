import { NgModule } from '@angular/core';
import { VideoApresentacaoComponent } from './video-apresentacao.component';

@NgModule({
  declarations: [VideoApresentacaoComponent],
  exports: [VideoApresentacaoComponent]
})
export class VideoApresentacaoModule { }
