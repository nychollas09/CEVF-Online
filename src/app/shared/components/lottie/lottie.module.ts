import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LottieAnimationViewModule } from 'ng-lottie';
import { LottieComponent } from './lottie.component';

@NgModule({
  declarations: [LottieComponent],
  imports: [
    CommonModule,
    LottieAnimationViewModule.forRoot()
  ],
  exports: [LottieComponent]
})
export class LottieModule { }
