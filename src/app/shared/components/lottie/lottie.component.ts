import { Component, EventEmitter, Input, Output } from '@angular/core';
import { LottieConfig } from 'src/app/models/domains/LottieConfig';

@Component({
  selector: 'app-lottie',
  templateUrl: './lottie.component.html',
  styleUrls: ['./lottie.component.scss']
})
export class LottieComponent {

  @Input() lottieConfig: LottieConfig = new LottieConfig();
  @Input() largura: number;
  @Input() altura: number;
  @Input() centralizar = false;
  @Input() exibir = true;
  @Output() animacaoCriada$: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  public handleAnimation(animacao: any): void {
    this.animacaoCriada$.emit(animacao);
  }

}
