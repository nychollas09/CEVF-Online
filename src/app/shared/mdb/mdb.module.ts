import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  ButtonsModule,
  CollapseModule,
  IconsModule,
  InputsModule,
  InputUtilitiesModule,
  ModalModule,
  WavesModule
} from 'angular-bootstrap-md';

@NgModule({
  declarations: [],
  exports: [
    CommonModule, InputsModule,
    InputUtilitiesModule,
    ButtonsModule, WavesModule, CollapseModule,
    ModalModule, IconsModule
  ]
})
export class MdbModule { }
