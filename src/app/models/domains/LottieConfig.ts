export class LottieConfig {
  public path: string;
  public renderer: string;
  public autoplay: boolean;
  public loop: boolean;
  public container?: HTMLDivElement

  constructor(init?: Partial<LottieConfig>) { }
}
