import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InicioComponent } from './inicio.component';
import { InicioRoutingModule } from './inicio.routing';
import { VideoApresentacaoModule } from 'src/app/shared/components/video-apresentacao/video-apresentacao.module';

@NgModule({
  declarations: [InicioComponent],
  imports: [
    CommonModule,
    InicioRoutingModule,
    VideoApresentacaoModule
  ]
})
export class InicioModule { }
