import { NgModule } from '@angular/core';
import { MdbModule } from 'src/app/shared/mdb/mdb.module';
import { FooterComponent } from './footer.component';

@NgModule({
  declarations: [FooterComponent],
  imports: [
    MdbModule
  ],
  exports: [FooterComponent]
})
export class FooterModule { }
