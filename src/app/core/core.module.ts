import { NgModule } from '@angular/core';
import { LottieModule } from '../shared/components/lottie/lottie.module';
import { FooterModule } from './footer/footer.module';
import { NavbarModule } from './navbar/navbar.module';

@NgModule({
  imports: [
    LottieModule,
    NavbarModule,
    FooterModule
  ],
  exports: [
    LottieModule,
    NavbarModule,
    FooterModule
  ]
})
export class CoreModule { }
