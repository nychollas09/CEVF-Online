import { NgModule } from '@angular/core';
import { MdbModule } from 'src/app/shared/mdb/mdb.module';
import { NavbarComponent } from './navbar.component';
import { NavbarModule as NavBarModuleMDB } from 'angular-bootstrap-md';

@NgModule({
  declarations: [NavbarComponent],
  imports: [
    MdbModule,
    NavBarModuleMDB
  ],
  exports: [
    NavbarComponent
  ]
})
export class NavbarModule { }
