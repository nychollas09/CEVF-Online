import { AfterViewInit, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { interval } from 'rxjs';
import { take } from 'rxjs/operators';
import { LottieConfig } from './models/domains/LottieConfig';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  public visivel = true;

  public lottieConfig: LottieConfig = {
    autoplay: true,
    loop: true,
    renderer: 'svg',
    path: '../assets/animations/onibus.json'
  };

  constructor(private router: Router, private activatedRoute: ActivatedRoute) { }

  ngAfterViewInit(): void {
    this._definirQuandoAnimacaoVaiAcabar();
  }

  private _definirQuandoAnimacaoVaiAcabar(): void {
    interval(4000).pipe(take(1)).subscribe(() => {
      this.visivel = false;
      this.router.navigate(['inicio'], { relativeTo: this.activatedRoute });
    });
  }

}
